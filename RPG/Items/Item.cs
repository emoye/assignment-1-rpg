﻿namespace RPG
{
    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemRequiredLevel { get; set; }
        public ItemSlot ItemSlot { get; set; }
    }
    public enum ItemSlot { Head, Body, Legs, WeaponSlot}
}
