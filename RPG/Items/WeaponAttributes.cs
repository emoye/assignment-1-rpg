﻿namespace RPG
{
    public class WeaponAttributes
    {
        public int BaseDamage { get; set; }
        public double AttackSpeed { get; set; }
    }
}