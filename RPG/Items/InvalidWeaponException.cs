﻿using System;

namespace RPG
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string message) : base(message)
        {
        }
        public override string Message => "Invalid weapon exception";
    }
}
