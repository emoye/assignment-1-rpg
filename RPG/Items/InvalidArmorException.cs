﻿using System;

namespace RPG
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string message) : base(message)
        {
        }
        public override string Message => "Invalid armor exception";
    }
}

