﻿namespace RPG
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        /// <summary>
        /// Calculates weapon DPS based on weapon attributes
        /// </summary>
        /// <returns>Weapon DPS</returns>
        public double GetWeaponDPS()
        {
            return WeaponAttributes.AttackSpeed * WeaponAttributes.BaseDamage;
        }
    }
    public enum WeaponType { Axe, Bow, Dagger, Hammer, Staff, Sword, Wand}
}
