﻿namespace RPG
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes ArmorAttributes { get; set; }
    }
    public enum ArmorType { Cloth, Leather, Mail, Plate};
}
