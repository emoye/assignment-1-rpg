﻿using System;

namespace RPG
{
    class Program
    {
        static void Main(string[] args)
        {

           // Creating a hero and some items and trying some methods
            Mage mage = new Mage()
            {
                HeroName = "Embla"
            };

            Weapon staff = new Weapon()
            {
                ItemName = "common staff",
                ItemRequiredLevel = 3,
                ItemSlot = ItemSlot.WeaponSlot,
                WeaponType = WeaponType.Staff,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 11, AttackSpeed = 5.7 }
            };

            Armor cloth = new Armor()
            {
                ItemName = "common cloth armor",
                ItemRequiredLevel = 2,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorType.Cloth,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 5, Strength = 2 }
            };

            mage.DisplayStats();

            mage.LevelUp();

            mage.DisplayStats();

            try
            {
                mage.EquipArmor(cloth);
            }
            catch
            {

            }

            mage.DisplayStats();

            mage.LevelUp(3);

            try
            {
                mage.EquipWeapon(staff);
            }
            catch
            {

            }

            mage.DisplayStats();
        }
    }
}
