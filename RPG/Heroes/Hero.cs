﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public abstract class Hero
    {
        public string HeroName { get; set; }
        public int HeroLevel { get; set; } = 1;
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; } = new PrimaryAttributes();
        public SecondaryAttributes SecondaryAttributes { get; set; }
        public Dictionary<ItemSlot, Item> Inventory { get; set; } = new Dictionary<ItemSlot, Item>();

        // Method to level up a hero 
        public abstract void LevelUp(int lvl);
        // Method to equip a hero with a weapon
        public abstract string EquipWeapon(Weapon weapon);
        // Method to equip a hero with armor
        public abstract string EquipArmor(Armor armor);
        // Method to calculate hero DPS
        public abstract double GetHeroDPS();

        /// <summary>
        /// Calculate and set secondary attributes based on primary attributes
        /// </summary>
        /// <param name="attributes"></param>
        public void SetSecondaryAttributes(PrimaryAttributes attributes)
        {
            SecondaryAttributes.Health = attributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = attributes.Strength + attributes.Dexterity;
            SecondaryAttributes.ElementalResistance = attributes.Intelligence;
        }

        /// <summary>
        /// Method to update total primary attributes
        /// </summary>
        public void UpdateTotalPrimaryAttributes(PrimaryAttributes attributes)
        {
            TotalPrimaryAttributes.Vitality += attributes.Vitality;
            TotalPrimaryAttributes.Strength += attributes.Strength;
            TotalPrimaryAttributes.Dexterity += attributes.Dexterity;
            TotalPrimaryAttributes.Intelligence += attributes.Intelligence;
        }

        /// <summary>
        /// Method to display hero stats, where the values change as the hero levels up and equips new items.
        /// </summary>
        public void DisplayStats()
        {
           // Create an object of the StringBuilder class
            StringBuilder sb = new StringBuilder();

            // Append all stats to sb
            sb.AppendLine();
            sb.AppendLine("HERO STATS");
            sb.AppendLine("-----------------------------------------------------------------------");
            sb.AppendLine($"Name: {HeroName}");
            sb.AppendLine($"Level: {HeroLevel}");
            sb.AppendLine($"Strength: {TotalPrimaryAttributes.Strength}");
            sb.AppendLine($"Dexterity: {TotalPrimaryAttributes.Dexterity}");
            sb.AppendLine($"Intelligence: {TotalPrimaryAttributes.Intelligence}");
            sb.AppendLine($"Health: {SecondaryAttributes.Health}");
            sb.AppendLine($"Armor rating: {SecondaryAttributes.ArmorRating}");
            sb.AppendLine($"Elemental resistance: {SecondaryAttributes.ElementalResistance}");
            sb.AppendLine($"Damage per second (DPS): {GetHeroDPS()}");
            sb.AppendLine("-----------------------------------------------------------------------");

            // Print stats
            Console.WriteLine(sb);
        }
    }
}
