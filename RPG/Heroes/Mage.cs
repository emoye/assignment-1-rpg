﻿using System;

namespace RPG
{
    public class Mage : Hero
    {
        public Mage()
        {
            BasePrimaryAttributes = new PrimaryAttributes
            {
                Vitality = 5,
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };
            UpdateTotalPrimaryAttributes(BasePrimaryAttributes);
            SecondaryAttributes = new SecondaryAttributes();
            SetSecondaryAttributes(TotalPrimaryAttributes);
        }

        /// <summary>
        /// Check if weapon is a valid WeaponType and has a valid level.
        /// If valid, add weapon to ItemSlot.weaponSlot in Inventory.
        /// </summary>
        /// <param name="weapon">Weapon to be added to Inventory</param>
        /// <returns>Success message</returns>
        /// <exception cref="InvalidWeaponException">If the WeaponType is not valid for Mage or if the WeaponLevel is too high</exception>
        public override string EquipWeapon(Weapon weapon)
        {
            // Check if valid weapon type and valid level
            if ((weapon.WeaponType == WeaponType.Staff || weapon.WeaponType == WeaponType.Wand) && weapon.ItemRequiredLevel <= HeroLevel)
            {
                // If valid, equip weapon, print message and return success message
                Inventory[ItemSlot.WeaponSlot] = weapon;
                Console.WriteLine($"{HeroName} has successfully equipped {weapon.ItemName} with level {weapon.ItemRequiredLevel}");
                return "New weapon equipped!";
            }
            else
            {
                // If not valid, throw exception
                throw new InvalidWeaponException("Invalid weapon");
            }
        }

        /// <summary>
        /// Check if armor is a valid ArmorType and has a valid level.
        /// If valid, add armor to ItemSlot in Inventory and update TotalPrimaryAttributes.
        /// </summary>
        /// <param name="armor">Armor to be equipped</param>
        /// <returns>Success message</returns>
        /// <exception cref="InvalidArmorException">If the ArmorType is not valid for Mage or if the WeaponLevel is too high</exception>
        public override string EquipArmor(Armor armor)
        {
            // Check if valid armor type and valid level
            if (armor.ArmorType == ArmorType.Cloth && armor.ItemRequiredLevel <= HeroLevel)
            {
                // If valid - equip armor, update total primary and secondary attributes, print a message and return success message
                Inventory[armor.ItemSlot] = armor;
                UpdateTotalPrimaryAttributes(armor.ArmorAttributes);
                SetSecondaryAttributes(TotalPrimaryAttributes);
                Console.WriteLine($"{HeroName} has successfully equipped {armor.ItemName} with level {armor.ItemRequiredLevel} on {armor.ItemSlot}");
                return "New armor equipped!";
            }
            else
            {
                // If not valid, throw exception
                throw new InvalidArmorException("Invalid armor");
            }
        }

        /// <summary>
        /// Takes in amount of levels to level up the hero.
        /// Increases base primary attributes and updates total primary attributes and secondary attributes accordingly.
        /// </summary>
        /// <param name="lvl">Amount of levels to level up, default is 1 level</param>
        /// <exception cref="ArgumentException">If the input (lvl) is 0 or less</exception>
        public override void LevelUp(int lvl = 1)
        {
            // Check if input level is less than 1, if true throw exception
            if (lvl < 1)
            {
                throw new ArgumentException("Hero cannot gain 0 or less levels");
            }

            // else, update hero level and primary/secondary attributes
            HeroLevel += lvl;
            BasePrimaryAttributes.Vitality += lvl * 3;
            BasePrimaryAttributes.Strength += lvl * 1;
            BasePrimaryAttributes.Dexterity += lvl * 1;
            BasePrimaryAttributes.Intelligence += lvl * 5;
            UpdateTotalPrimaryAttributes(new PrimaryAttributes { Vitality = lvl * 3, Strength = lvl * 1, Dexterity = lvl * 1, Intelligence = lvl * 5 });
            SetSecondaryAttributes(TotalPrimaryAttributes);
            Console.WriteLine($"{HeroName} leveled up {lvl} level(s)!");
        }

        /// <summary>
        /// Calculate hero DPS based on equipped weapon and total primary attributes.
        /// </summary>
        /// <returns>Hero DPS</returns>
        public override double GetHeroDPS()
        {
            // Is true if hero has a item equipped in weapon slot
            bool foundItem = Inventory.TryGetValue(ItemSlot.WeaponSlot, out Item equippedItem);

            // If hero has item equipped in weapon slot
            if (foundItem)
            {
                // Cast item to weapon type
                Weapon equippedWeapon = (Weapon)equippedItem;
                // Get the DPS of the weapon
                double weaponDPS = equippedWeapon.GetWeaponDPS();
                // Use weapon DPS to calculate hero DPS and return
                return weaponDPS * (1 + ((double)TotalPrimaryAttributes.Intelligence / 100.0));
            }
            // If no weapon equipped, calculate without weapon DPS
            return 1 + (TotalPrimaryAttributes.Intelligence / 100);
        }
    }
}
