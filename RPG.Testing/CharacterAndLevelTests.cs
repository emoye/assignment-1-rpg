using System;
using Xunit;

namespace RPG.Testing
{
    public class CharacterAndLevelTests
    {
        /// <summary>
        /// Test if a hero is level 1 when created
        /// </summary>
        [Fact]
        public void Constructor_CreateNewMage_LevelShouldBe1()
        {
            // Arrange
            Mage mage = new Mage();
            int expectedLevel = 1;

            // Act
            int actualLevel = mage.HeroLevel;

            // Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        /// <summary>
        /// Test if when a hero gains a level, the level is 2
        /// </summary>
        [Fact]
        public void LevelUp_MageLevelUp_LevelShouldBe2()
        {
            // Arrange
            Mage mage = new Mage();
            int expectedLevel = 2;

            // Act
            mage.LevelUp(1);
            int actualLevel = mage.HeroLevel;

            // Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        /// <summary>
        /// Test if an ArgumentException is thrown when trying to gain 0 or less levels
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_MageLevelUpZeroOrLessLevels_ArgumentExceptionShouldBeThrown(int input)
        {
            // Arrange
            Mage mage = new Mage();

            // Act and assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(input));
        }

        /// <summary>
        /// Test if Mage is created with the proper default attributes
        /// </summary>
        [Fact]
        public void Constructor_CreateNewMage_ShouldHaveCorrectDefaultAttributes()
        {
            // Arrange
            Mage mage = new Mage();
            int expectedVitality = 5;
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedIntelligence = 8;

            // Act
            int actualVitality = mage.BasePrimaryAttributes.Vitality;
            int actualStrength = mage.BasePrimaryAttributes.Strength;
            int actualDexterity = mage.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = mage.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test if Ranger is created with the proper default attributes
        /// </summary>
        [Fact]
        public void Constructor_CreateNewRanger_ShouldHaveCorrectDefaultAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger();
            int expectedVitality = 8;
            int expectedStrength = 1;
            int expectedDexterity = 7;
            int expectedIntelligence = 1;

            // Act
            int actualVitality = ranger.BasePrimaryAttributes.Vitality;
            int actualStrength = ranger.BasePrimaryAttributes.Strength;
            int actualDexterity = ranger.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = ranger.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test if Rogue is created with the proper default attributes
        /// </summary>
        [Fact]
        public void Constructor_CreateNewRogue_ShouldHaveCorrectDefaultAttributes()
        {
            // Arrange
            Rogue rogue = new Rogue();
            int expectedVitality = 8;
            int expectedStrength = 2;
            int expectedDexterity = 6;
            int expectedIntelligence = 1;

            // Act
            int actualVitality = rogue.BasePrimaryAttributes.Vitality;
            int actualStrength = rogue.BasePrimaryAttributes.Strength;
            int actualDexterity = rogue.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = rogue.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test if Warrior is created with the proper default attributes
        /// </summary>
        [Fact]
        public void Constructor_CreateNewWarrior_ShouldHaveCorrectDefaultAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior();
            int expectedVitality = 10;
            int expectedStrength = 5;
            int expectedDexterity = 2;
            int expectedIntelligence = 1;

            // Act
            int actualVitality = warrior.BasePrimaryAttributes.Vitality;
            int actualStrength = warrior.BasePrimaryAttributes.Strength;
            int actualDexterity = warrior.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = warrior.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test that when a Mage levels up, the attribute increase is correct
        /// </summary>
        [Fact]
        public void LevelUp_MageLevelUp_ShouldHaveCorrectPrimaryAttributesIncrease()
        {
            // Arrange
            Mage mage = new Mage();
            int expectedVitality = 5 + 3;
            int expectedStrength = 1 + 1;
            int expectedDexterity = 1 + 1;
            int expectedIntelligence = 8 + 5;

            // Act
            mage.LevelUp(1);
            int actualVitality = mage.BasePrimaryAttributes.Vitality;
            int actualStrength = mage.BasePrimaryAttributes.Strength;
            int actualDexterity = mage.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = mage.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test that when a Ranger levels up, the attribute increase is correct
        /// </summary>
        [Fact]
        public void LevelUp_RangerLevelUp_ShouldHaveCorrectPrimaryAttributesIncrease()
        {
            // Arrange
            Ranger ranger = new Ranger();
            int expectedVitality = 8 + 2;
            int expectedStrength = 1 + 1;
            int expectedDexterity = 7 + 5;
            int expectedIntelligence = 1 + 1;

            // Act
            ranger.LevelUp(1);
            int actualVitality = ranger.BasePrimaryAttributes.Vitality;
            int actualStrength = ranger.BasePrimaryAttributes.Strength;
            int actualDexterity = ranger.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = ranger.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test that when a Rogue levels up, the attribute increase is correct
        /// </summary>
        [Fact]
        public void LevelUp_RogueLevelUp_ShouldHaveCorrectPrimaryAttributesIncrease()
        {
            // Arrange
            Rogue rogue = new Rogue();
            int expectedVitality = 8 + 3;
            int expectedStrength = 2 + 1;
            int expectedDexterity = 6 + 4;
            int expectedIntelligence = 1 + 1;

            // Act
            rogue.LevelUp(1);
            int actualVitality = rogue.BasePrimaryAttributes.Vitality;
            int actualStrength = rogue.BasePrimaryAttributes.Strength;
            int actualDexterity = rogue.BasePrimaryAttributes.Dexterity;
            int actualIntelligence = rogue.BasePrimaryAttributes.Intelligence;

            // Assert
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        /// <summary>
        /// Test that when a Warrior levels up, the attribute increase is correct
        /// </summary>
        [Fact]
        public void LevelUp_WarriorLevelUp_ShouldHaveCorrectPrimaryAttributesIncrease()
        {
            // Arrange
            Warrior warrior = new Warrior();
            int expectedVitality = 10 + 5;
            int expectedStrength = 5 + 3;
            int expectedDexterity = 2 + 2;
            int expectedIntelligence = 1 + 1;

            // Act
            warrior.LevelUp(1);

            // Assert
            Assert.Equal(expectedVitality, warrior.BasePrimaryAttributes.Vitality);
            Assert.Equal(expectedStrength, warrior.BasePrimaryAttributes.Strength);
            Assert.Equal(expectedDexterity, warrior.BasePrimaryAttributes.Dexterity);
            Assert.Equal(expectedIntelligence, warrior.BasePrimaryAttributes.Intelligence);
        }

        /// <summary>
        /// Test if secondary stats are calculated from levelled up hero
        /// </summary>
        [Fact]
        public void LevelUp_WarriorLevelUp_ShouldHaveCorrectSecondaryAttributesIncrease()
        {
            // Arrange
            Warrior warrior = new Warrior();
            int expectedHealth = 150;
            int expectedArmorRating = 12;
            int expectedElementalResistance = 2;

            // Act
            warrior.LevelUp(1);
            int actualHealth = warrior.SecondaryAttributes.Health;
            int actualArmorRating = warrior.SecondaryAttributes.ArmorRating;
            int actualElementalResistance = warrior.SecondaryAttributes.ElementalResistance;

            // Assert
            Assert.Equal(expectedHealth, actualHealth);
            Assert.Equal(expectedArmorRating, actualArmorRating);
            Assert.Equal(expectedElementalResistance, actualElementalResistance);
        }
    }
}
