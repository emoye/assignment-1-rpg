﻿using System;
using Xunit;

namespace RPG.Testing
{
    public class ItemsAndEquipmentTests
    {
        /// <summary>
        /// Test if when a hero tries to equip a weapon with too high level, InvalidWeaponException is thrown
        /// </summary>
        [Fact]
        public void EquipWeapon_WarriorEquipsWeaponWithTooHighLevel_InvalidWeaponExceptionShouldBeThrown()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemRequiredLevel = 2,
                ItemSlot = ItemSlot.WeaponSlot,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1 }
            };

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }

        /// <summary>
        /// Test if when a hero tries to equip armor with too high level, InvalidArmorException is thrown
        /// </summary>
        [Fact]
        public void EquipArmor_WarriorEquipsArmorWithTooHighLevel_InvalidArmorExceptionShouldBeThrown()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Armor testPlate = new Armor()
            {
                ItemName = "Common plate armor",
                ItemRequiredLevel = 2,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            // Act and assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlate));
        }

        /// <summary>
        /// Test if when a hero tries to equip the wrong weapon type, InvalidWeaponException is thrown
        /// </summary>
        [Fact]
        public void EquipWeapon_WarriorEquipsBow_InvalidWeaponExceptionShouldBeThrown()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.WeaponSlot,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 12, AttackSpeed = 0.8 }
            };

            // Act and assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }

        /// <summary>
        /// Test if when a hero tries to equip wrong armor type, InvalidArmorException is thrown
        /// </summary>
        [Fact]
        public void EquipArmor_WarriorEquipsClothArmor_InvalidArmorExceptionShouldBeThrown()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Armor testCloth = new Armor()
            {
                ItemName = "Common cloth",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.Head,
                ArmorType = ArmorType.Cloth,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 1, Strength = 5 }
            };

            // Act and assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testCloth));
        }

        /// <summary>
        /// Test if when a hero equips a valid weapon, a success message is returned
        /// </summary>
        [Fact]
        public void EquipWeapon_WarriorEquipsValidWeapon_ShouldReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.WeaponSlot,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1 }
            };
            string expectedMessage = "New weapon equipped!";

            // Act
            string actualMessage = warrior.EquipWeapon(testAxe);

            // Act and assert
            Assert.Equal(expectedMessage, actualMessage);
        }

        /// <summary>
        /// Test if when a hero equips a valid armor, a success message is returned
        /// </summary>
        [Fact]
        public void EquipArmor_WarriorEquipsValidArmor_ShouldReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Armor testPlate = new Armor()
            {
                ItemName = "Common plate armor",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            string expectedMessage = "New armor equipped!";

            // Act
            string actualMessage = warrior.EquipArmor(testPlate);

            // Act and assert
            Assert.Equal(expectedMessage, actualMessage);
        }

        /// <summary>
        /// Test if DPS is correctly calculated when no weapon is equipped
        /// </summary>
        [Fact]
        public void GetHeroDPS_WarriorWithNoWeaponEquipped_ShouldReturnCorrectDPS()
        {
            // Arrange
            Warrior warrior = new Warrior();
            double expectedDPS = 1+(5/100);

            // Act
            double actualDPS = warrior.GetHeroDPS();

            // Act and assert
            Assert.Equal(expectedDPS, actualDPS);
        }

        /// <summary>
        /// Test if DPS is correctly calculated when valid weapon equipped
        /// </summary>
        [Fact]
        public void GetHeroDPS_WarriorWithValidWeaponEquipped_ShouldReturnCorrectDPS()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.WeaponSlot,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1 }
            };
            double expectedDPS = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));

            // Act
            warrior.EquipWeapon(testAxe);
            double actualDPS = warrior.GetHeroDPS();

            // Act and assert
            Assert.Equal(expectedDPS, actualDPS);
        }

        /// <summary>
        /// Test if DPS is correctly calculated when valid weapon and valid armor is equipped
        /// </summary>
        [Fact]
        public void GetHeroDPS_WarriorWithValidWeaponAndArmorEquipped_ShouldReturnCorrectDPS()
        {
            // Arrange
            Warrior warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.WeaponSlot,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { BaseDamage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlate = new Armor()
            {
                ItemName = "Common plate armor",
                ItemRequiredLevel = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            double expectedDPS = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0));

            // Act
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlate);
            double actualDPS = warrior.GetHeroDPS();

            // Act and assert
            Assert.Equal(expectedDPS, actualDPS);
        }
    }
}
